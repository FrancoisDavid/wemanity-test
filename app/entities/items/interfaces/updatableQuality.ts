// Interface Helping Composition implementation, specify behaviour to be implemented
export interface IUpdatableQuality {
    updateQuality(): void;
}