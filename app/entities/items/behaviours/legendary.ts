import { Item } from '../item';
import { IUpdatableQuality } from '../interfaces/updatableQuality';

// Implement behaviour for Legendary Items
export class Legendary extends Item implements IUpdatableQuality {
    updateQuality = () => {
        this.sellIn -= 1;
    }
}