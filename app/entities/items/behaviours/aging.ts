import { Item } from '../item';
import { IUpdatableQuality } from '../interfaces/updatableQuality';

// Constant representing MAX VALUE for quality
const MAX_QUALITY = 50;

// Implement behaviour for AgingFine Items
export class AgingFine extends Item implements IUpdatableQuality {
    
    updateQuality = () => {
        const upgradeSpeed = (this.sellIn > 0) ? 1 : 2;

        this.quality = (this.quality + upgradeSpeed < MAX_QUALITY) ? this.quality + upgradeSpeed : MAX_QUALITY;
        this.sellIn -= 1
    };
}