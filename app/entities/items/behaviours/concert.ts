import { Item } from '../item';
import { IUpdatableQuality } from '../interfaces/updatableQuality';

// Constant representing MAX VALUE for quality
const MAX_QUALITY = 50;

// Implement behaviour for Concert Items
export class Concert extends Item implements IUpdatableQuality {

    updateQuality = () => {

        if (this.quality < MAX_QUALITY) {
            this.quality += 1;
        }

        if (this.sellIn < 11 && this.quality < MAX_QUALITY) {
            this.quality += 1;
        }
        
        if (this.sellIn < 6 && this.quality < MAX_QUALITY) {
            this.quality += 1;
        } 

        this.sellIn -= 1;

        if (this.sellIn < 0) {
            this.quality = 0;
        }
    }
}