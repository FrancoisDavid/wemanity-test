import { Item } from '../item';
import { IUpdatableQuality } from '../interfaces/updatableQuality';

// Implement behaviour for Conjured Items
export class Conjured extends Item implements IUpdatableQuality {

    updateQuality = () => {
        const degradeSpeed = (this.sellIn > 0) ? 2 : 4;

        this.quality = (this.quality - degradeSpeed >= 0) ? this.quality - degradeSpeed : 0;
        this.sellIn -= 1;
    };
}