import { Item } from '../item';
import { IUpdatableQuality } from '../interfaces/updatableQuality';

// Implement behaviour for Standard Items
export class Standard extends Item implements IUpdatableQuality {

    updateQuality = () => {
        const degradeSpeed = (this.sellIn - 1 > 0) ? 1 : 2;

        this.quality = (this.quality - degradeSpeed >= 0) ? this.quality - degradeSpeed : 0;
        this.sellIn -= 1
    };
}