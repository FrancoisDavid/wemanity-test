
import { Item } from './entities/items/item';
import { Concert } from './entities/items/behaviours/concert';
import { AgingFine } from './entities/items/behaviours/aging';
import { Standard } from './entities/items/behaviours/standard';
import { Conjured } from './entities/items/behaviours/conjured';
import { Legendary } from './entities/items/behaviours/legendary';

// Type syntaxic sugar
type Updatable = AgingFine | Standard | Conjured | Legendary | Concert;

/**
 * Determines the Type of Item depending on its name
 * 
 * @param item The Item to be wrapped
 * @returns The Wrapped Item
 */
function updatableBuilder(item: Item): Updatable {
    let wrappedItem: Updatable;

    switch (item.name) {
        case 'Aged Brie':
            wrappedItem = new AgingFine(item.name, item.sellIn, item.quality);
            break;
        case 'Backstage passes to a TAFKAL80ETC concert':
            wrappedItem = new Concert(item.name, item.sellIn, item.quality);
            break;
        case 'Conjured Mana Cake':
            wrappedItem = new Conjured(item.name, item.sellIn, item.quality);
            break;
        case 'Sulfuras, Hand of Ragnaros':
            wrappedItem = new Legendary(item.name, item.sellIn, item.quality);
            break;
        default:
            wrappedItem = new Standard(item.name, item.sellIn, item.quality);
            break;
    }

    return wrappedItem;
}

export class GildedRose {
    items: Array<Updatable> = [];

    constructor(items = [] as Array<Item>) {
        items.forEach((item: Item) => this.items = [ ...this.items, updatableBuilder(item) ]);
    }

    /**
     * @returns An array of updated Items
     */
    updateQuality = (): Array<Item> =>
        this.items.map((item: Updatable) => {
            item.updateQuality();

            // Re-instanciate Item in order to get Rid of updateQuality function
            return new Item(item.name, item.sellIn, item.quality);
        });

}
