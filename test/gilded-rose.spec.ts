import { expect } from 'chai';
import { GildedRose } from '../app/gilded-rose';
import { Item } from '../app/entities/items/item';

const items = [
    new Item("+5 Dexterity Vest", 10, 20), //
    new Item("Aged Brie", 2, 0), //
    new Item("Elixir of the Mongoose", 5, 7), //
    new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
    new Item("Sulfuras, Hand of Ragnaros", -1, 80),
    new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
    new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
    new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49)
];

const resultItems = [
    new Item("+5 Dexterity Vest", 9, 19), //
    new Item("Aged Brie", 1, 1), //
    new Item("Elixir of the Mongoose", 4, 6), //
    new Item("Sulfuras, Hand of Ragnaros", -1, 80), //
    new Item("Sulfuras, Hand of Ragnaros", -2, 80),
    new Item("Backstage passes to a TAFKAL80ETC concert", 14, 21),
    new Item("Backstage passes to a TAFKAL80ETC concert", 9, 50),
    new Item("Backstage passes to a TAFKAL80ETC concert", 4, 50)
]

describe('Gilded Rose', function () {

    it('should foo', function() {
        const gildedRose = new GildedRose([ new Item('foo', 0, 0) ]);
        const items = gildedRose.updateQuality();
        expect(items[0].name).to.equal('foo');
    });

    // STANDARD ITEM TESTS
    it ('should udpate standard item quality', function() {
        const gildedRose = new GildedRose([ new Item('foo', 1, 1 ) ]) ;
        const items = gildedRose.updateQuality();

        expect(items).to.eql([{
            'name': 'foo',
            'quality': 0,
            'sellIn': 0
        }]);
    });

    it ('should not update if quality <= 0', function() {
        const gildedRose = new GildedRose([ new Item('foo', 1, 0 ) ]) ;
        const items = gildedRose.updateQuality();

        expect(items).to.eql([{
            'name': 'foo',
            'quality': 0,
            'sellIn': 0
        }]);
    });

    it ('should update standard twice as fast if sellIn < 0', function() {
        const gildedRose = new GildedRose([new Item('foo', 0, 2)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'foo',
            'quality': 0,
            'sellIn': -1
        }]);
    });

    it ('Standard should not go under 0 quality', function() {
        const gildedRose = new GildedRose([new Item('foo', 1, 0)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'foo',
            'quality': 0,
            'sellIn': 0
        }]);
    });

    // CONJURED ITEMS TESTS
    it ('should update conjured', function() {
        const gildedRose = new GildedRose([new Item('Conjured Mana Cake', 1, 2)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Conjured Mana Cake',
            'quality': 0,
            'sellIn': 0
        }]);
    });


    it ('should update conjured twice as fast if sellIn < 0', function() {
        const gildedRose = new GildedRose([new Item('Conjured Mana Cake', 0, 4)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Conjured Mana Cake',
            'quality': 0,
            'sellIn': -1
        }]);
    });

    it ('Conjured should not go under 0 quality', function() {
        const gildedRose = new GildedRose([new Item('Conjured Mana Cake', 1, 1)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Conjured Mana Cake',
            'quality': 0,
            'sellIn': 0
        }]);
    });

    // CONCERT ITEMS TESTS
    it ('should update concert normally', function() {
        const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 15, 0)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Backstage passes to a TAFKAL80ETC concert',
            'quality': 1,
            'sellIn': 14
        }]);
    });

    it ('should update concert twice as fast', function() {
        const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 10, 0)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Backstage passes to a TAFKAL80ETC concert',
            'quality': 2,
            'sellIn': 9
        }]);
    });

    it ('should update concert thrice as fast', function() {
        const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 5, 0)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Backstage passes to a TAFKAL80ETC concert',
            'quality': 3,
            'sellIn': 4
        }]);
    });

    it ('should update concert to 0', function() {
        const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 0, 1)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Backstage passes to a TAFKAL80ETC concert',
            'quality': 0,
            'sellIn': -1
        }]);
    });

    it ('Concert should not go over 50 quality', function() {
        const gildedRose = new GildedRose([new Item('Backstage passes to a TAFKAL80ETC concert', 1, 49)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Backstage passes to a TAFKAL80ETC concert',
            'quality': 50,
            'sellIn': 0
        }]);
    });

    // AGING ITEMS TESTS
    it ('should update AgingFine', function() {
        const gildedRose = new GildedRose([new Item('Aged Brie', 1, 0)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Aged Brie',
            'quality': 1,
            'sellIn': 0
        }]);
    });

    it ('should update AgingFine twice as fast', function() {
        const gildedRose = new GildedRose([new Item('Aged Brie', 0, 0)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Aged Brie',
            'quality': 2,
            'sellIn': -1
        }]);
    });

    it ('Aging should not go over 50 quality', function() {
        const gildedRose = new GildedRose([ new Item('Aged Brie', 1, 50)]);
        const items = gildedRose.updateQuality();
        expect(items).to.eql([{
            'name': 'Aged Brie',
            'quality': 50,
            'sellIn': 0
        }]);
    });

    // UPDATE QUALITY TESTS
    it ('Should update all correctly', function() {
        const gildedRose = new GildedRose(items);
        const itemss = gildedRose.updateQuality();
        expect(itemss).to.eql(resultItems);
    });
});
