# wemanity - test

Technical test for Wemanity appliance

To test project after cloning repo :

```
npm install
npm run test
```


## Choices

At first I made the choice to take a dumb approach to the problem and get rid of the IF forest in the UpdateQuality method.

Therefore you will see in the earlier commits that I only took the logic off the method and implemented it in separated methods inside the GildedRose Class.

The idea was to first separate logic by item type to see if a pattern was emerging.

The fact that those different Item, from the same Entity, were treated the same way by updating all qualities at once, but having different behaviours depending on their name (and logically "subtypes"), made me think about using Composition to solve the problem.

The idea being calling all the different updateQuality methods the same way, avoiding unecessary and hard to read if/else statements.

Therefore I wrote an Interface encapsulating the contract of the behaviour to be determined : 

```Typescript
export interface IUpdatableQuality {
    updateQuality(): void;
}
```

NOTE : I only wrote a single level on interface for this exercise. But if we want to start defining more complex behaviours with several methods we should implement 2 level of interface. One like the one above, defining a single method, and another composed of several methods to be defined describing the real "behaviour" of the Item.

Then, to implement those different behaviours, I wrote a set of classes inheriting from Item and containing only the implementation of the said behaviours. (Those classes may not be named so accurately to be honest).

```Typescript
// Implement behaviour for Standard Items
export class Standard extends Item implements IUpdatableQuality {

    updateQuality = () => {
        const degradeSpeed = (this.sellIn - 1 > 0) ? 1 : 2;

        this.quality = (this.quality - degradeSpeed >= 0) ? this.quality - degradeSpeed : 0;
        this.sellIn -= 1
    };
}
```

If the behaviour was determined by more than one interface, we would see here the Standard class implements a Second-level interface encapsulating all the methods to be implemented, and the class actually implementing more than one method.


Finally, the initial UpdateQuality method being called with a type Item, and returning the same, I wrap and unwrap the actual items.

Thanks to Typescript DuckTyping, I could have only return an array of Updatable since they all match at least the properties defined by Item (name, sellIn and quality). But it would have meant returning more than expecting since the updateQuality method is not a part of item in the first place.
So I prefered rebuilding it, since it was more logical, for example in the situation of an API you don't want an object to be consumed and returned with new unknown and useless to you properties.
It is kind of hazardous though, since the user may want someday to define an updateQuality method himself.

In the end we now updateQuality the same way for each Item, only difference being that when we first instantiate a gildedRose, a method wrap the Item and thus, add to it the proper method.




## Architecture

After struggling a bit about this particular point, I chosed to put all the behaviours inside and items dir along with its interfaces, as all those part are depending only on the Item class.

Separating behaviours in different files may seem a bit tedious but it allowsd to add more of it easily.


